/**
 * Build this example on linux with:
 * ./waf configure --enable-examples --enable-if-kiss --with-driver-usart=linux --enable-crc32 clean build
 */

#include <stdio.h>
#include <csp/csp.h>
#include <csp/interfaces/csp_if_kiss.h>

#include <csp/drivers/usart.h>
#include <csp/arch/csp_thread.h>

#define PORT 10
#define MY_ADDRESS 1

#define SERVER_TIDX 0
#define CLIENT_TIDX 1
#define USART_HANDLE 0

CSP_DEFINE_TASK(task_server) {
    int running = 1;
    // Create a CSP socket, with no socket options
    // csp_socket_t is a csp_conn_s defined in csp_conn.h
    csp_socket_t *socket = csp_socket(CSP_SO_NONE);
    csp_conn_t *conn;
    csp_packet_t *packet; //csp_packet_t definded in csp.h
    csp_packet_t *response;

    // Get a pointer to a free csp_packet_t
    // Not sure what the number 2 is for - perhaps because we are putting 2 bytes in the response
    response = csp_buffer_get(sizeof(csp_packet_t) + 2);
    if( response == NULL ) {
        fprintf(stderr, "Could not allocate memory for response packet!\n");
        return CSP_TASK_RETURN;
    }
    response->data[0] = 'O';
    response->data[1] = 'K';
    response->length = 2;

    // Bind socket to all incoming ports, including csp service ports.
    csp_bind(socket, CSP_ANY);
    // listen for incoming connections, with a queue length of 5
    csp_listen(socket, 5);

    printf("Server task started\r\n");

    while(running) {
    	// Wait for a new connection on a socket created by csp_socket, wait 10000 ms
        if( (conn = csp_accept(socket, 10000)) == NULL ) {
            continue;
        }

        // read incoming packets on the connection, timeout 100 ms
        // note returned packet must be freed via csp_buffer_free or by reusing it for csp_send
        while( (packet = csp_read(conn, 100)) != NULL ) {
        	// switch on destination port of the connection
            switch( csp_conn_dport(conn) ) {
                case PORT:
                    if( packet->data[0] == 'q' ) // q means quit
                        running = 0;
                    // free the packet we just read
                    csp_buffer_free(packet);
                    // send a response (OK - set above), 1000 ms timeout
                    // Note: if this fails, must free the "frame"? yourself; clearly this is not done here
                    csp_send(conn, response, 1000);
                    break;
                default:
                	// for handling a csp service port.  The packet will be used or deleted.
                    csp_service_handler(conn, packet);
                    break;
            }
        }

        csp_close(conn); //Closes a given connection and frees buffers used.
    }
    // free up the response packet
    csp_buffer_free(response);

    return CSP_TASK_RETURN;
}

CSP_DEFINE_TASK(task_client) {

    char outbuf = 'q';
    char inbuf[3] = {0};
    int pingResult;

    for(int i = 50; i <= 200; i+= 50) {
    	// send a ping, to MY_ADDRESS, 1000 ms timeout, 100 byte packet, no connection options
    	// pingResult of -1 is an error.
        pingResult = csp_ping(MY_ADDRESS, 1000, 100, CSP_O_NONE);
        printf("Ping with payload of %d bytes, took %d ms\n", i, pingResult);
        csp_sleep_ms(1000);
    }
    // requesting info about the os the node is running on (linux in this case)
    // Each call prints to the console
    csp_ps(MY_ADDRESS, 1000); // request process list (which is not available on linux);1000ms timeout
    csp_sleep_ms(1000);
    csp_memfree(MY_ADDRESS, 1000); // request amount of free memory for a node. 1000ms timeout
    csp_sleep_ms(1000);
    csp_buf_free(MY_ADDRESS, 1000); // request number of free buffers for a node, 1000 ms timeout
    csp_sleep_ms(1000);
    csp_uptime(MY_ADDRESS, 1000); // request uptime
    csp_sleep_ms(1000);
    //Perform an entire request/reply transaction
    // * Copies both input buffer and reply to output buffeer.
    // * Also makes the connection and closes it again
    // priority 0, dest, port, timeout, output buffer, length of output, input buffer, inptut length
    // This is set up to send 'q', which should quit the server
    csp_transaction(0, MY_ADDRESS, PORT, 1000, &outbuf, 1, inbuf, 2);
    printf("Quit response from server: %s\n", inbuf);

    return CSP_TASK_RETURN;
}

int main(int argc, char **argv) {
    csp_debug_toggle_level(CSP_PACKET);
    csp_debug_toggle_level(CSP_INFO);

    // Start buffer handling;  10 buffers, 300 bytes long each
    csp_buffer_init(10, 300);
    // start up CSP
    csp_init(MY_ADDRESS);

    struct usart_conf conf;

#if defined(CSP_WINDOWS)
    conf.device = argc != 2 ? "COM4" : argv[1];
    conf.baudrate = CBR_9600;
    conf.databits = 8;
    conf.paritysetting = NOPARITY;
    conf.stopbits = ONESTOPBIT;
    conf.checkparity = FALSE;
#elif defined(CSP_POSIX)
    conf.device = argc != 2 ? "/dev/ttyUSB0" : argv[1];
    conf.baudrate = 115200;
#elif defined(CSP_MACOSX)
    conf.device = argc != 2 ? "/dev/tty.usbserial-FTSM9EGE" : argv[1];
    conf.baudrate = 115200;
#endif

	/* Run USART init */
	usart_init(&conf);

    /* Setup CSP interface */
	static csp_iface_t csp_if_kiss;
	static csp_kiss_handle_t csp_kiss_driver;
	// initialize the KISS interface
	csp_kiss_init(&csp_if_kiss, &csp_kiss_driver, usart_putc, usart_insert, "KISS");
		
	/* Setup callback from USART RX to KISS RS */
	void my_usart_rx(uint8_t * buf, int len, void * pxTaskWoken) {
		csp_kiss_rx(&csp_if_kiss, buf, len, pxTaskWoken);
	}
	usart_set_callback(my_usart_rx);

    csp_route_set(MY_ADDRESS, &csp_if_kiss, CSP_NODE_MAC);
    csp_route_start_task(0, 0);

    csp_conn_print_table();
    csp_route_print_table();
    csp_route_print_interfaces();

    // start the server task
    csp_thread_handle_t handle_server;
    csp_thread_create(task_server, (signed char *) "SERVER", 1000, NULL, 0, &handle_server);
    csp_thread_handle_t handle_client;
    csp_thread_create(task_client, (signed char *) "CLIENT", 1000, NULL, 0, &handle_client);

    /* Wait for program to terminate (ctrl + c) */
    while(1) {
    	csp_sleep_ms(1000000);
    }

    return 0;

}

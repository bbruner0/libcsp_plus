/*
 * house_keeper.c
 *
 *  Created on: Dec 30, 2014
 *      Author: albertasat
 */
#include "house_keeper.h"

void HouseKeeper_setTestValues(eps_hk_t* test_data){
	uint8_t i;
	test_data->vboost[0] = 1;
	test_data->vboost[1] = 2;
	test_data->vboost[2] = 3;
	test_data->vbatt = 4;
	test_data->curin[0] = 5;
	test_data->curin[1] = 6;
	test_data->curin[2] = 7;
	test_data->cursun = 8;
	test_data->cursys = 9;
	test_data->reserved1 = 10;
	for (i = 0; i < 6; i++){
		test_data->curout[i] = i + 11;
	}
	for (i = 0; i < 8; i++){
		test_data->output[i] = i + 17;
	}
	for (i = 0; i < 8; i++){
		test_data->output_on_delta[i] = i + 25;
	}
	for (i = 0; i < 8; i++){
		test_data->output_off_delta[i] = i + 33;
	}
	for (i = 0; i < 6; i++){
		test_data->latchup[i] = i + 41;
	}
	test_data->wdt_i2c_time_left = 47;
	test_data->wdt_gnd_time_left = 48;
	test_data->wdt_csp_pings_left[0] = 49;
	test_data->wdt_csp_pings_left[1] = 50;
	test_data->counter_wdt_i2c = 51;
	test_data->counter_wdt_gnd = 52;
	test_data->counter_wdt_csp[0] = 53;
	test_data->counter_wdt_csp[1] = 54;
	test_data->counter_boot = 55;
	for (i = 0; i < 6; i++){
		test_data->temp[i] = i + 56;
	}
	test_data->bootcause = 62;
	test_data->battmode = 63;
	test_data->pptmode = 64;
	test_data->reserved2 = 65;
}


bool HouseKeeper_equals(eps_hk_t* hk1, eps_hk_t* hk2){
	bool equals = true;
	uint8_t i;
	for (i = 0; i < 3; i++){
		equals = equals && (hk1->vboost[i] == hk2->vboost[i]);
	}
	equals = equals && (hk1->vbatt == hk2->vbatt);
	for (i = 0; i < 3; i++){
		equals = equals && (hk1->curin[i] == hk2->curin[i]);
	}
	equals = equals && (hk1->cursun == hk2->cursun);
	equals = equals && (hk1->cursys == hk2->cursys);
	equals = equals && (hk1->reserved1 == hk2->reserved1);
	for (i = 0; i < 6; i++){
		equals = equals && (hk1->curout[i] == hk2->curout[i]);
	}
	for (i = 0; i < 8; i++){
		equals = equals && (hk1->output[i] == hk2->output[i]);
	}
	for (i = 0; i < 8; i++){
		equals = equals && (hk1->output_on_delta[i] == hk2->output_on_delta[i]);
	}
	for (i = 0; i < 8; i++){
		equals = equals && (hk1->output_off_delta[i] == hk2->output_off_delta[i]);
	}
	for (i = 0; i < 6; i++){
		equals = equals && (hk1->latchup[i] == hk2->latchup[i]);
	}
	equals = equals && (hk1->wdt_i2c_time_left == hk2->wdt_i2c_time_left);
	equals = equals && (hk1->wdt_gnd_time_left == hk2->wdt_gnd_time_left);
	equals = equals && (hk1->wdt_csp_pings_left[0] == hk2->wdt_csp_pings_left[0]);
	equals = equals && (hk1->wdt_csp_pings_left[1] == hk2->wdt_csp_pings_left[1]);
	equals = equals && (hk1->counter_wdt_i2c == hk2->counter_wdt_i2c);
	equals = equals && (hk1->counter_wdt_gnd == hk2->counter_wdt_gnd);
	equals = equals && (hk1->counter_wdt_csp[0] == hk2->counter_wdt_csp[0]);
	equals = equals && (hk1->counter_wdt_csp[1] == hk2->counter_wdt_csp[1]);
	equals = equals && (hk1->counter_boot == hk2->counter_boot);
	for (i = 0; i < 6; i++){
		equals = equals && (hk1->temp[i] == hk2->temp[i]);
	}
	equals = equals && (hk1->bootcause == hk2->bootcause);
    equals = equals && (hk1->battmode == hk2->battmode);
    equals = equals && (hk1->pptmode == hk2->pptmode);
    equals = equals && (hk1->reserved2 == hk2->reserved2);

	return equals;
}
/*
uint16_t vboost[3]; //! Voltage of boost converters [mV] [PV1, PV2, PV3]
uint16_t vbatt; //! Voltage of battery [mV]
uint16_t curin[3]; //! Current in [mA]
uint16_t cursun; //! Current from boost converters [mA]
uint16_t cursys; //! Current out of battery [mA]
uint16_t reserved1; //! Reserved for future use
uint16_t curout[6]; //! Current out (switchable outputs) [mA]
uint8_t output[8]; //! Status of outputs**
uint16_t output_on_delta[8]; //! Time till power on** [s]
uint16_t output_off_delta[8]; //! Time till power off** [s]
uint16_t latchup[6]; //! Number of latch-ups
uint32_t wdt_i2c_time_left; //! Time left on I2C wdt [s]
uint32_t wdt_gnd_time_left; //! Time left on I2C wdt [s]
uint8_t wdt_csp_pings_left[2]; //! Pings left on CSP wdt
uint32_t counter_wdt_i2c; //! Number of WDT I2C reboots
uint32_t counter_wdt_gnd; //! Number of WDT GND reboots
uint32_t counter_wdt_csp[2]; //! Number of WDT CSP reboots
uint32_t counter_boot; //! Number of EPS reboots
int16_t temp[6]; //! Temperatures [degC] [0 = TEMP1, TEMP2, TEMP3, TEMP4, BP4a, BP4b]*
uint8_t bootcause; //! Cause of last EPS reset
uint8_t battmode; //! Mode for battery [0 = initial, 1 = undervoltage, 2 = nominal, 3 = batteryfull]
uint8_t pptmode; //! Mode of PPT tracker [1=MPPT, 2=FIXED]
uint16_t reserved2;
*/


